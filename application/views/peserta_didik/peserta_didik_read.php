<!doctype html>
<html>

<head>
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>" />
    <style>
        body {
            padding: 15px;
        }
    </style>
</head>

<body>
    <h2 style="margin-top:0px">Peserta_didik Read</h2>
    <table class="table">
        <tr>
            <td>Nama Peserta Didik</td>
            <td><?php echo $nama_peserta_didik; ?></td>
        </tr>
        <tr>
            <td>Jenis kelamin</td>
            <td><?php echo $jkelamin; ?></td>
        </tr>
        <tr>
            <td>Tgl Lahir</td>
            <td><?php echo $tgl_lahir; ?></td>
        </tr>
        <tr>
            <td>Polda Asal</td>
            <td><?php echo $polda_asal; ?></td>
        </tr>
        <tr>
            <td>Nosis</td>
            <td><?php echo $nosis; ?></td>
        </tr>
        <tr>
            <td>Id Kelas</td>
            <td><?php echo $id_kelas; ?></td>
        </tr>
        <tr>
            <td></td>
            <td><a href="<?php echo site_url('peserta_didik') ?>" class="btn btn-default">Cancel</a></td>
        </tr>
    </table>
</body>

</html>
<div class="content-wrapper">

	<section class="content">
		<div class="box box-warning box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">INPUT DATA PESERTA DIDIK</h3>
			</div>
			<form action="<?php echo $action; ?>" method="post">

				<table class='table table-bordered>' <tr>
					<td width='200'>Nama Peserta Didik <?php echo form_error('nama_peserta_didik') ?></td>
					<td><input type="text" class="form-control" name="nama_peserta_didik" id="nama_peserta_didik" placeholder="Nama Peserta Didik" value="<?php echo $nama_peserta_didik; ?>" /></td>
					</tr>
					<tr>
						<td width='200'>jenis Kelamin <?php echo form_error('jkelamin') ?></td>
						<td><input type="text" class="form-control" name="jkelamin" id="jkelamin" placeholder="Jenis kelamin" value="<?php echo $jkelamin; ?>" /></td>
					</tr>
					<tr>
						<td width='200'>Tgl Lahir <?php echo form_error('tgl_lahir') ?></td>
						<td><input type="date" class="form-control" name="tgl_lahir" id="tgl_lahir" placeholder="Tgl Lahir" value="<?php echo $tgl_lahir; ?>" /></td>
					</tr>
					<tr>
						<td width='200'>Polda Asal <?php echo form_error('polda_asal') ?></td>
						<td><input type="text" class="form-control" name="polda_asal" id="polda_asal" placeholder="Polda Asal" value="<?php echo $polda_asal; ?>" /></td>
					</tr>
					<tr>
						<td width='200'>Nosis <?php echo form_error('nosis') ?></td>
						<td><input type="text" class="form-control" name="nosis" id="nosis" placeholder="Nosis" value="<?php echo $nosis; ?>" /></td>
					</tr>
					<tr>
						<td width='200'>Nama Kelas <?php echo form_error('id_kelas') ?></td>
						<td><?php echo cmb_dinamis('id_kelas', 'kelas', 'nama_kelas', 'id', $id_kelas) ?></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="hidden" name="id" value="<?php echo $id; ?>" />
							<button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i> <?php echo $button ?></button>
							<a href="<?php echo site_url('peserta_didik') ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> Kembali</a></td>
					</tr>
				</table>
			</form>
		</div>
</div>
</div>
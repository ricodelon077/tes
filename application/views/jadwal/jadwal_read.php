<!doctype html>
<html>

<head>
    <title>harviacode.com - codeigniter crud generator</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>" />
    <style>
        body {
            padding: 15px;
        }
    </style>
</head>

<body>
    <h2 style="margin-top:0px">Jadwal Read</h2>
    <table class="table">
        <tr>
            <td>Id Kelas</td>
            <td><?php echo $id_kelas; ?></td>
        </tr>
        <tr>
            <td>Id Mapel</td>
            <td><?php echo $id_mapel; ?></td>
        </tr>
        <tr>
            <td>Hari</td>
            <td><?php echo $hari; ?></td>
        </tr>
        <tr>
            <td>Id Pendidik</td>
            <td><?php echo $id_pendidik; ?></td>
        </tr>
        <tr>
            <td>Jam Mulai</td>
            <td><?php echo $jam_mulai; ?></td>
        </tr>
        <tr>
            <td>Jam Akhir</td>
            <td><?php echo $jam_akhir; ?></td>
        </tr>
        <tr>
            <td></td>
            <td><a href="<?php echo site_url('jadwal') ?>" class="btn btn-default">Cancel</a></td>
        </tr>
    </table>
</body>

</html>
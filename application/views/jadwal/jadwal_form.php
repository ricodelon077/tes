<div class="content-wrapper">

	<section class="content">
		<div class="box box-warning box-solid">
			<div class="box-header with-border">
				<h3 class="box-title">INPUT DATA JADWAL</h3>
			</div>
			<form action="<?php echo $action; ?>" method="post">

				<table class='table table-bordered>' <tr>

					<td width='200'>Kelas <?php echo form_error('id_kelas') ?></td>
					<td><?php echo cmb_dinamis('id_kelas', 'kelas', 'nama_kelas', 'id') ?></td>
					</tr>

					<tr>
						<td>Mata Pelajaran</td>
						<td><?php echo cmb_dinamis('id_mapel', 'mapel', 'nama_mapel', 'id') ?></td>
					</tr>
					<tr>
						<td width='200'>Hari <?php echo form_error('hari') ?></td>
						<td><input type="text" class="form-control" name="hari" id="hari" placeholder="Hari" value="<?php echo $hari; ?>" /></td>
					</tr>
					<tr>
						<td width='200'>Pendidik <?php echo form_error('id_pendidik') ?></td>
						<td><?php echo cmb_dinamis('id_pendidik', 'tbl_user', 'full_name', 'id_users') ?></td>
					</tr>
					<tr>
						<td width='200'>Jam Mulai <?php echo form_error('jam_mulai') ?></td>
						<td><input type="time" class="form-control" name="jam_mulai" id="jam_mulai" placeholder="Jam Mulai" value="<?php echo $jam_mulai; ?>" /></td>
					</tr>
					<tr>
						<td width='200'>Jam Akhir <?php echo form_error('jam_akhir') ?></td>
						<td><input type="time" class="form-control" name="jam_akhir" id="jam_akhir" placeholder="Jam Akhir" value="<?php echo $jam_akhir; ?>" /></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="hidden" name="id" value="<?php echo $id; ?>" />
							<button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i> <?php echo $button ?></button>
							<a href="<?php echo site_url('jadwal') ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> Kembali</a></td>
					</tr>
				</table>
			</form>
		</div>
</div>
</div>
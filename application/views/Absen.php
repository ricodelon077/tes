<div class="content-wrapper">
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"> SELAMAT MENGAJAR</h3>
            </div>
            <table class="table table-bordered" style="margin-bottom: 10px">
                <tr>
                    <th>No</th>
                    <th>Kelas</th>
                    <th>Mata Pelajaran</th>
                    <th>Hari</th>
                    <th>Pendidik</th>
                    <th>Jam Mulai</th>
                    <th>Jam Akhir</th>
                    <th>

                    </th>
                </tr><?php
                        foreach ($jadwal_data as $jadwal) {
                        ?>
                    <tr>
                        <td width="10px"><?php echo ++$start ?></td>
                        <td><?php echo $jadwal->nama_kelas ?></td>
                        <td><?php echo $jadwal->nama_mapel ?></td>
                        <td><?php echo $jadwal->hari ?></td>
                        <td><?php echo $jadwal->full_name ?></td>
                        <td><?php echo $jadwal->jam_mulai ?></td>
                        <td><?php echo $jadwal->jam_akhir ?></td>
                        <td style="text-align:center" width="200px">
                            <?php
                            echo anchor(site_url('absen/masuk/' . $jadwal->id), '<i class="masuk" aria-hidden="true">MASUK</i>', 'class="btn-sm btn-primary"');
                            echo '  ';

                            ?>
                        </td>
                    </tr>
                <?php
                        }
                ?>
            </table>
            <div class="row">
                <div class="col-md-6">

                </div>
                <div class="col-md-6 text-right">
                    <?php echo $pagination ?>
                </div>
            </div>
        </div>
</div>
</div>
</div>
</section>
</div>
</div>
</section>
</div>
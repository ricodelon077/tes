<?php
class Absen extends CI_Controller
{
    function __construct()
    {

        parent::__construct();
        is_login();
        $this->load->model('Jadwal_model');
        $this->load->library('form_validation');
    }
    public function index()

    {
        $id = $this->session->userdata('id_users');
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->uri->segment(3));

        if ($q <> '') {
            $config['base_url'] = base_url() . '.php/c_url/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'index.php/jadwal/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'index.php/jadwal/index/';
            $config['first_url'] = base_url() . 'index.php/jadwal/index/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = FALSE;
        $config['total_rows'] = $this->Jadwal_model->total_rows_id($id, $q);
        $jadwal = $this->Jadwal_model->getjadwalbypendidik($id, $config['per_page'], $start, $q);
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'jadwal_data' => $jadwal,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template', 'Absen', $data);
    }

    public function masuk()
    {
        echo "test";
    }
}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Peserta_didik extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Peserta_didik_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->uri->segment(3));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . '.php/c_url/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'index.php/peserta_didik/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'index.php/peserta_didik/index/';
            $config['first_url'] = base_url() . 'index.php/peserta_didik/index/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = FALSE;
        $config['total_rows'] = $this->Peserta_didik_model->total_rows($q);
        $peserta_didik = $this->Peserta_didik_model->get_limit_data($config['per_page'], $start, $q);
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'peserta_didik_data' => $peserta_didik,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template','peserta_didik/peserta_didik_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Peserta_didik_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'nama_peserta_didik' => $row->nama_peserta_didik,
		'jkelamin' => $row->jkelamin,
		'tgl_lahir' => $row->tgl_lahir,
		'polda_asal' => $row->polda_asal,
		'nosis' => $row->nosis,
		'id_kelas' => $row->id_kelas,
	    );
            $this->template->load('template','peserta_didik/peserta_didik_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('peserta_didik'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('peserta_didik/create_action'),
	    'id' => set_value('id'),
	    'nama_peserta_didik' => set_value('nama_peserta_didik'),
	    'jkelamin' => set_value('jkelamin'),
	    'tgl_lahir' => set_value('tgl_lahir'),
	    'polda_asal' => set_value('polda_asal'),
	    'nosis' => set_value('nosis'),
	    'id_kelas' => set_value('id_kelas'),
	);
        $this->template->load('template','peserta_didik/peserta_didik_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama_peserta_didik' => $this->input->post('nama_peserta_didik',TRUE),
		'jkelamin' => $this->input->post('jkelamin',TRUE),
		'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
		'polda_asal' => $this->input->post('polda_asal',TRUE),
		'nosis' => $this->input->post('nosis',TRUE),
		'id_kelas' => $this->input->post('id_kelas',TRUE),
	    );

            $this->Peserta_didik_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('peserta_didik'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Peserta_didik_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('peserta_didik/update_action'),
		'id' => set_value('id', $row->id),
		'nama_peserta_didik' => set_value('nama_peserta_didik', $row->nama_peserta_didik),
		'jkelamin' => set_value('jkelamin', $row->jkelamin),
		'tgl_lahir' => set_value('tgl_lahir', $row->tgl_lahir),
		'polda_asal' => set_value('polda_asal', $row->polda_asal),
		'nosis' => set_value('nosis', $row->nosis),
		'id_kelas' => set_value('id_kelas', $row->id_kelas),
	    );
            $this->template->load('template','peserta_didik/peserta_didik_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('peserta_didik'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'nama_peserta_didik' => $this->input->post('nama_peserta_didik',TRUE),
		'jkelamin' => $this->input->post('jkelamin',TRUE),
		'tgl_lahir' => $this->input->post('tgl_lahir',TRUE),
		'polda_asal' => $this->input->post('polda_asal',TRUE),
		'nosis' => $this->input->post('nosis',TRUE),
		'id_kelas' => $this->input->post('id_kelas',TRUE),
	    );

            $this->Peserta_didik_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('peserta_didik'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Peserta_didik_model->get_by_id($id);

        if ($row) {
            $this->Peserta_didik_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('peserta_didik'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('peserta_didik'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_peserta_didik', 'nama peserta didik', 'trim|required');
	$this->form_validation->set_rules('jkelamin', 'jkelamin', 'trim|required');
	$this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required');
	$this->form_validation->set_rules('polda_asal', 'polda asal', 'trim|required');
	$this->form_validation->set_rules('nosis', 'nosis', 'trim|required');
	$this->form_validation->set_rules('id_kelas', 'id kelas', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Peserta_didik.php */
/* Location: ./application/controllers/Peserta_didik.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-06-25 09:44:40 */
/* http://harviacode.com */
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siswa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Siswa_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->uri->segment(3));

        if ($q <> '') {
            $config['base_url'] = base_url() . '.php/c_url/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'index.php/siswa/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'index.php/siswa/index/';
            $config['first_url'] = base_url() . 'index.php/siswa/index/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = FALSE;
        $config['total_rows'] = $this->Siswa_model->total_rows($q);
        $siswa = $this->Siswa_model->get_limit_data($config['per_page'], $start, $q);
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'siswa_data' => $siswa,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template', 'siswa/siswa_list', $data);
    }

    public function read($id)
    {
        $row = $this->Siswa_model->get_by_id($id);
        if ($row) {
            $data = array(
                'nosis' => $row->nis,
                'nama_peserta_didik' => $row->nama_peserta_didik,
                'jkelamin' => $row->jkelamin,
                'polda_asal' => $row->polda_asal,
                'tgl_lahir' => $row->tgl_lahir,

            );
            $this->template->load('template', 'siswa/siswa_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('siswa'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('siswa/create_action'),
            'nosis' => set_value('nosis'),
            'nama_peserta_didik' => set_value('nama_pserta_didik'),
            'jkelamin' => set_value('jkelamin'),
            'polda_asal' => set_value('polda_asal'),
            'tgl_lahir' => set_value('tgl_lahir'),

        );
        $this->template->load('template', 'siswa/siswa_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nama_peserta_didik' => $this->input->post('nama_peserta_didik', TRUE),
                'jkelamin' => $this->input->post('jkelamin', TRUE),
                'polda_asal' => $this->input->post('polda_asal', TRUE),
                'tgl_lahir' => $this->input->post('tgl_lahir', TRUE),

            );

            $this->Siswa_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('siswa'));
        }
    }

    public function update($id)
    {
        $row = $this->Siswa_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('siswa/update_action'),
                'nosis' => set_value('nis', $row->nosis),
                'nama_peserta_didik' => set_value('nama_peserta_didik', $row->nama_peserta_didik),
                'jkelamin' => set_value('jkelamin', $row->jkelamin),
                'polda_asal' => set_value('nohp_ortu', $row->polda_asal),
                'tgl_lahir' => set_value('tgl_lahir', $row->tgl_lahir),

            );
            $this->template->load('template', 'siswa/siswa_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('siswa'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('nis', TRUE));
        } else {
            $data = array(
                'nama_peserta_didik' => $this->input->post('nama_peserta_didik', TRUE),
                'jkelamin' => $this->input->post('jkelamin', TRUE),
                'polda_asal' => $this->input->post('polda_asal', TRUE),
                'tgl_lahir' => $this->input->post('tgl_lahir', TRUE),

            );

            $this->Siswa_model->update($this->input->post('nis', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('siswa'));
        }
    }

    public function delete($id)
    {
        $row = $this->Siswa_model->get_by_id($id);

        if ($row) {
            $this->Siswa_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('siswa'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('siswa'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nama_peserta_didik', 'nama_peserta_didik', 'trim|required');
        $this->form_validation->set_rules('jkelamin', 'jkelamin', 'trim|required');
        $this->form_validation->set_rules('polda_asal', 'polda_asal', 'trim|required');
        $this->form_validation->set_rules('tgl_lahir', 'tgl lahir', 'trim|required');

        $this->form_validation->set_rules('nosis', 'nosis', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file Siswa.php */
/* Location: ./application/controllers/Siswa.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-06-15 10:02:32 */
/* http://harviacode.com */
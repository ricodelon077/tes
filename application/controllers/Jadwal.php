<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jadwal extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Jadwal_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->uri->segment(3));

        if ($q <> '') {
            $config['base_url'] = base_url() . '.php/c_url/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'index.php/jadwal/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'index.php/jadwal/index/';
            $config['first_url'] = base_url() . 'index.php/jadwal/index/';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = FALSE;
        $config['total_rows'] = $this->Jadwal_model->total_rows($q);
        $jadwal = $this->Jadwal_model->get_limit_data($config['per_page'], $start, $q);
        $config['full_tag_open'] = '<ul class="pagination pagination-sm no-margin pull-right">';
        $config['full_tag_close'] = '</ul>';
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'jadwal_data' => $jadwal,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->template->load('template', 'jadwal/jadwal_list', $data);
    }

    public function read($id)
    {
        $row = $this->Jadwal_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'id_kelas' => $row->id_kelas,
                'id_mapel' => $row->id_mapel,
                'hari' => $row->hari,
                'id_pendidik' => $row->id_pendidik,
                'jam_mulai' => $row->jam_mulai,
                'jam_akhir' => $row->jam_akhir,
            );
            $this->template->load('template', 'jadwal/jadwal_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('jadwal/create_action'),
            'id' => set_value('id'),
            'id_kelas' => set_value('id_kelas'),
            'id_mapel' => set_value('id_mapel'),
            'hari' => set_value('hari'),
            'id_pendidik' => set_value('id_pendidik'),
            'jam_mulai' => set_value('jam_mulai'),
            'jam_akhir' => set_value('jam_akhir'),
        );
        $this->template->load('template', 'jadwal/jadwal_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        //if ($this->form_validation->run() == FALSE) {
        //     $this->create();
        // } else {
        $data = array(
            'id_kelas' => $this->input->post('id_kelas', TRUE),
            'id_mapel' => $this->input->post('id_mapel', TRUE),
            'hari' => $this->input->post('hari', TRUE),
            'id_pendidik' => $this->input->post('id_pendidik', TRUE),
            'jam_mulai' => $this->input->post('jam_mulai', TRUE),
            'jam_akhir' => $this->input->post('jam_akhir', TRUE),
        );

        $this->Jadwal_model->insert($data);
        $this->session->set_flashdata('message', 'Create Record Success 2');
        redirect(site_url('jadwal'));
        //  }
    }

    public function update($id)
    {
        $row = $this->Jadwal_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('jadwal/update_action'),
                'id' => set_value('id', $row->id),
                'id_kelas' => set_value('id_kelas', $row->id_kelas),
                'id_mapel' => set_value('id_mapel', $row->id_mapel),
                'hari' => set_value('hari', $row->hari),
                'id_pendidik' => set_value('id_pendidik', $row->id_pendidik),
                'jam_mulai' => set_value('jam_mulai', $row->jam_mulai),
                'jam_akhir' => set_value('jam_akhir', $row->jam_akhir),
            );
            $this->template->load('template', 'jadwal/jadwal_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'id_kelas' => $this->input->post('id_kelas', TRUE),
                'id_mapel' => $this->input->post('id_mapel', TRUE),
                'hari' => $this->input->post('hari', TRUE),
                'id_pendidik' => $this->input->post('id_pendidik', TRUE),
                'jam_mulai' => $this->input->post('jam_mulai', TRUE),
                'jam_akhir' => $this->input->post('jam_akhir', TRUE),
            );

            $this->Jadwal_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('jadwal'));
        }
    }

    public function delete($id)
    {
        $row = $this->Jadwal_model->get_by_id($id);

        if ($row) {
            $this->Jadwal_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('jadwal'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('jadwal'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_kelas', 'id kelas', 'trim|required');
        $this->form_validation->set_rules('id_mapel', 'id mapel', 'trim|required');
        $this->form_validation->set_rules('hari', 'hari', 'trim|required');
        $this->form_validation->set_rules('id_pendidik', 'id pendidik', 'trim|required');
        $this->form_validation->set_rules('jam_mulai', 'jam mulai', 'trim|required');
        $this->form_validation->set_rules('jam_akhir', 'jam akhir', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}

/* End of file Jadwal.php */
/* Location: ./application/controllers/Jadwal.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-06-15 09:54:58 */
/* http://harviacode.com */
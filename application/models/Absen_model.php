<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Absen_model extends CI_Model
{

    public $table = 'absen pendidik';
    public $id = 'jadwal.id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
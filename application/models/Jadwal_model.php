<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jadwal_model extends CI_Model
{

    public $table = 'jadwal';
    public $id = 'jadwal.id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->like('id', $q);
        $this->db->or_like('id_kelas', $q);
        $this->db->or_like('id_mapel', $q);
        $this->db->or_like('hari', $q);
        $this->db->or_like('id_pendidik', $q);
        $this->db->or_like('jam_mulai', $q);
        $this->db->or_like('jam_akhir', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function total_rows_id($id, $q = NULL)
    {
        $this->db->like('id', $q);
        $this->db->or_like('id_kelas', $q);
        $this->db->or_like('id_mapel', $q);
        $this->db->or_like('hari', $q);
        $this->db->or_like('id_pendidik', $q);
        $this->db->or_like('jam_mulai', $q);
        $this->db->or_like('jam_akhir', $q);
        $this->db->from($this->table);
        $this->db->where('id_pendidik', $id);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('jadwal.id', $q);
        $this->db->or_like('id_kelas', $q);
        $this->db->or_like('id_mapel', $q);
        $this->db->or_like('hari', $q);
        $this->db->or_like('id_pendidik', $q);
        $this->db->or_like('jam_mulai', $q);
        $this->db->or_like('jam_akhir', $q);
        $this->db->limit($limit, $start);
        $this->db->join('kelas', 'kelas.id = jadwal.id_kelas');
        $this->db->join('mapel', 'mapel.id = jadwal.id_mapel');
        $this->db->join('tbl_user', 'tbl_user.id_users = jadwal.id_pendidik');
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
    function getjadwalbypendidik($id, $limit, $start = 0, $q = NULL)
    {
        $this->db->join('kelas', 'kelas.id = jadwal.id_kelas');
        $this->db->join('mapel', 'mapel.id = jadwal.id_mapel');
        $this->db->join('tbl_user', 'tbl_user.id_users = jadwal.id_pendidik');
        $this->db->where('jadwal.id_pendidik', $id);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
}

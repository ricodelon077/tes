<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Peserta_didik_model extends CI_Model
{

    public $table = 'peserta_didik';
    public $id = 'peserta_didik.id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->like('id', $q);
        $this->db->or_like('nama_peserta_didik', $q);
        $this->db->or_like('jkelamin', $q);
        $this->db->or_like('tgl_lahir', $q);
        $this->db->or_like('polda_asal', $q);
        $this->db->or_like('nosis', $q);
        $this->db->or_like('id_kelas', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('peserta_didik.id', $q);
        $this->db->or_like('nama_peserta_didik', $q);
        $this->db->or_like('jkelamin', $q);
        $this->db->or_like('tgl_lahir', $q);
        $this->db->or_like('polda_asal', $q);
        $this->db->or_like('nosis', $q);
        $this->db->or_like('id_kelas', $q);
        $this->db->limit($limit, $start);
        $this->db->join('kelas', 'kelas.id= peserta_didik.id_kelas');
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}
